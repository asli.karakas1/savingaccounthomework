package com.codistan.java.lesson10;

public class CheckingAccount extends BankAccount {
	
	public double minimumBalanceLimit =1500; // we created a field, this way we are not hard-coding.
	public double maximumTransactionLimit = 10000;
	public double accountBalanceMinimumThreshold =  (-500);
	

	public CheckingAccount(String accountNo, double balance, String userSSN) {
		super(accountNo, balance, userSSN);
		
	
//while creating a class, type in the superclass box the parent class name(the one you want to extend)
//we need to create a constructor to call the super constructor. Eclipse did it for us as shown above.
//you can add more inputs inside the parenthesis.
	}
//right click source-override/implement methods:
	@Override
	public void withdrawCash(double amount) {
if(checkMaximumTransactionAmount(amount) && this.checkAccBalMinimumThreashold(amount)) {	
   checkMinimumBalanceRule(amount);
   checkNegativeAccountBalance(amount);
   super.withdrawCash(amount);
   //this.addTransaction(amount);
}
	
		
	}

	@Override //annotation
	public void transferFundsInternally(double amount, String accountNumber) {
		if (this.checkMaximumTransactionAmount(amount)) {
			
			checkMinimumBalanceRule(amount);
		super.transferFundsInternally(amount, accountNumber);
		}
	}
	
	
	
	
	
	public void checkMinimumBalanceRule(double amount) {
		if(this.checkBalance()-amount <= this.minimumBalanceLimit) {
			this.chargeFee(15.00);
			
		}
	}
	
	public boolean checkMaximumTransactionAmount(double amount) {
		if(amount <= this.maximumTransactionLimit) {
				return true;
		} else {
			System.out.println("you are not allowed to use moore than " + this.maximumTransactionLimit);	
			return false;
		}
			
	}
	
	@Override
	public void depositCash(double amount) {
		// TODO Auto-generated method stub
		super.depositCash(amount);
		notifyIRSifNecessary(); 
	}
	@Override
	public void depositCheck(double amount, String accountNo, String routingNo) {
		// TODO Auto-generated method stub
		super.depositCheck(amount, accountNo, routingNo);
		notifyIRSifNecessary();
	}
	public void notifyIRSifNecessary() {
		if (this.checkBalance() > 10000)
		System.out.println("Hey IRS, following account has a balance more than 10K:" + this.getAccountNo());
	}

	public void checkNegativeAccountBalance(double amount) {
		if(this.checkBalance()<0) {
			this.chargeFee(35);
		}
	}

	public boolean checkAccBalMinimumThreashold(double amount) {
		if(this.checkBalance()- amount > this.accountBalanceMinimumThreshold) {
			return true;
		} else {
			System.out.println("Your final balance can not be less than -500");
			return false;
		}
	}
	
	
}
