package com.codistan.java.lesson10;
public class SimpleCalculator {
	
	double m = 0.0;  //double m; can we also use this way. It will take the default value on class level. When you use in local level, you have to assign value to it.
	double maxInputLimit = 1000;
	double minInputLimit = 0;
	final static double p=3.14; //static method so we can call on the SimpleCalculatorRunner as System.out.println(SimpleCalculator.pi); without the need 
	                            //static variable can't call an instance member.
	public void inputValidation(double input) {
		if( !(input<=maxInputLimit && input>=minInputLimit) ) {
			System.out.println("Invalid Input");
			System.exit(0);
		} 
	}
	
	public SimpleCalculator() {
		super();
		System.out.println("We are creating a copy of simple calculator");
	}
	
	public SimpleCalculator(double min, double max) {
		this.minInputLimit = min;
		this.maxInputLimit = max;  
	}
	
	
	
	public int SimpleCalculator(int a, int b) {
		return a+b;
		
	}
	public SimpleCalculator(double m, double minInputLimit, double maxInputLimit) {   //constructors should have the same name with class
		this(minInputLimit, maxInputLimit); 
		this.m = m;
		//this.minInputLimit = minInputLimit;
		//this.maxInputLimit = maxInputLimit;
		
		
	}

	public SimpleCalculator(double m) {
	
		this.m=m;
		
	}
	
	public double addNumbers(double num1, double num2) {
//		System.out.println(num1+num2);
		inputValidation(num1);
		inputValidation(num2);
		return num1+num2;

	}
	
	public double addNumbers(double num1, double num2, double num3) {
		return num1+num2+num3;
	}
	
	public double subtractNumbers(double num1, double num2) {
		return num1-num2;
	}
	
	public double multiplyNumbers(double num1, double num2) {
		return num1*num2;
	}
	
	public double diveNumbers(double num1, double num2) {
		double result = 0.0;
		inputValidation(num1);
		inputValidation(num2);
		if(num2!=0) {
		 result = num1/num2;
		 return result;
		} else {
			System.out.println("0 is not acceptable for the second number");
		}
		
		return result; 
		
	}
	
	public void addToMemory(double inputForMemory) {
		m = inputForMemory;
	}

	public double getM() {
		return m;
	}

	public void setM(double m) {
		this.m = m;
	}
	
	public double addNumbersAndSaveToTheMemory(double a, double b) {
		double total = addNumbers(a, b);
		setM(total);
		return total;
	}
	
	public void printMemory() {
		getM();
	}
	
	public void mPlus(double currentValue) {
//		setM(getM()+currentValue); 
		double total = getM() + currentValue; 
		setM(total); 
	}
	
	public void mMinus() {
		
	}
	
	public void mReset() {
		setM(0.0);
	}
	//Please add a method called squareArea() to the SimpleCalculator that calculates the area of a square.
	
}
