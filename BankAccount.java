package com.codistan.java.lesson10; //implementation-we are coding, designing something.

import java.util.ArrayList;

/* Requirements:

  1- if the balance gets lower than $1500 at any point, you will be charged for $15
  2- You can not withdraw more than $10,000 per transaction.
  3- If the balance is more $10,000, send a message to IRS.
  4- If the balance goes lower than zero, you will be charged for $35.
  5- The balance can not go less than -500.
  
 */

public class BankAccount { //right click, source, generate getters and setters and we selected all the fields (account no, balance, userSSN)
	//class level variables are called fields:
	private String accountNo;
	private double balance;
	private String userSSN;
	
	private ArrayList<Double> transactionHistory = new ArrayList<>();	//ArrayList cannot take primitive type, that's why I use reference type. It is the difference between array and array lists. arrays can take primitive type.
	
	//if we don't create a constructor, it will create a default constructor which we don't prefer in this case.
	//created constructors--> right click, source, generate constructors using fields. we chose the fields we will need to access from out database.
	public BankAccount(String accountNo, double balance, String userSSN) {
		super();//what is the super class of BankAccount class:BankAccount parent class is object class which has a constructor.
		this.accountNo = accountNo;
		this.balance = balance;
		this.userSSN = userSSN;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public double getBalance() {
	
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getUserSSN() {
		return userSSN;
	}
	public void setUserSSN(String userSSN) {
		this.userSSN = userSSN;
	}
	
	public double checkBalance() {
		System.out.println("Current Balance: " + this.getBalance());
		return this.getBalance();
	}
	
	public void depositCash(double amount) {
		this.setBalance(this.balance + amount );
		this.addTransaction(amount);
		//this is another way of stating:
		//double totalAmount=this.getBalance() + amount;
		//this.setBalance(totalAmount);
	}
	
	public void depositCheck(double amount, String accountNo, String routingNo) {
		
    this.setBalance(this.getBalance() + amount);
	
    //this is another way of stating:
	//double totalAmount = getBalance() + amount;
	//setBalance(totalAmount);
	
	this.addTransaction(amount);
	
	}

	public void withdrawCash(double amount) {
		this.setBalance(this.getBalance()-amount);
		
		this.addTransaction((amount) * (-1));
	}
	
	
	public void transferFundsInternally(double amount, String accountNumber) {
		this.setBalance(this.getBalance()-amount);
		this.addTransaction(amount);
		
	}
	
	public void addTransaction(double amount) {
		this.transactionHistory.add(amount);
		
	}
	
	public void showTransactions() {
		for (Double double1 : transactionHistory) {
			System.out.println("Transaction: " + double1);
		}
	}
	
	
	public void chargeFee(double fee) {
		
		this.setBalance(this.getBalance()-fee);
	}
    

	
	
	
	
	
	
	
	
	
	
	
	
	
}
