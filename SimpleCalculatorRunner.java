package com.codistan.java.lesson10;

public class SimpleCalculatorRunner { //class is called a blueprint.

	public static void main(String[] args) {
		
		SimpleCalculator myCalc = new SimpleCalculator(); 
		myCalc.addNumbers(5.0, 10.0);
		myCalc.addNumbers(11.1, 0.4);
		myCalc.subtractNumbers(10.0, 5);
		double tempNum = myCalc.addNumbers(2, 2);
		System.out.println(tempNum);
		byte i = (byte) tempNum; //explicit casting example
		System.out.println(i);
		
		System.out.println(myCalc.diveNumbers(10, 0));
		
		myCalc.addToMemory(myCalc.addNumbersAndSaveToTheMemory(2,3)); //m becomes 5
		System.out.println(myCalc.getM()); //prints m
		myCalc.mPlus(4); // add 4 to m resulting to 9
		System.out.println(myCalc.getM()); // prints m 
		myCalc.mPlus(5); // adds 5 to 9
		System.out.println(myCalc.getM()); //prints m
		
		
		

		System.out.println(myCalc.getM());
		
		//Class starts here on Saturday 		
		
				System.out.println(myCalc.addNumbers(2, 20)); //testing a positive test case with the first object
				SimpleCalculator myCalc2 = new SimpleCalculator(0, 10);
				System.out.println(myCalc2.addNumbers(2, 9)); //testing a positive test case
		//		System.out.println(myCalc2.addNumbers(2, 20)); //testing a negative test case
		
	
	SimpleCalculator memorySet = new SimpleCalculator(2, 0, 5);
	System.out.println(memorySet.addNumbers(2, 3));
	System.out.println(memorySet.getM());
	
	SmartCalculator smartCalculator = new SmartCalculator();
	System.out.println(smartCalculator.circleArea(3));
	
	
	

System.out.println(smartCalculator.addNumbers(2, 3));
	
	SmartCalculator mySmartCalc2 = new SmartCalculator(); //new object class created
	System.out.println(mySmartCalc2.toString());
	}
	
	
}
