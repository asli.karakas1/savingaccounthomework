package com.codistan.java.lesson10;
/*
	a. You can open a saving account at least with $10k (this should be handled by the constructor)
	b. If your balance becomes less than $10K at any point in time, the bank will charge you $35. 
	c. If your balance becomes more than $20K, the bank will add 5% of your balance to the balance. 
	d. Once your balance becomes less than $20K, the bank will charge you 10%. 
	e. If your balance becomes less than $10K anytime, the bank will charge you 20% and you will not able to do more transaction anymore. 
	f. Every transaction should be recorded and when we ask for the transaction history, it will print the transaction including all the charges that bank did or all the earnings so that user will know.
 */

public class SavingAccount extends BankAccount {

	public double minimumBalanceAmount = 10000;
	public double awardAmount = 20000;
	public double accountBalanceMinimumThreshold =  10000;
	


	public SavingAccount(String accountNo, double balance, String userSSN) {
		super(accountNo, balance, userSSN);
	

	}

	public boolean minimumSavingAccountLimit(double amount) {
		if (this.checkBalance() >= minimumBalanceAmount) {
			return true;
		} else {
			System.out.println("Minimum Saving Account balance can be $10,000.00");
			return false;
		}
	}

	@Override
	public void depositCash(double amount) {
	
			super.depositCash(amount);
		this.awardForTwentyK(amount);

	}

	@Override
	public void depositCheck(double amount, String accountNo, String routingNo) {
		if(minimumSavingAccountLimit(amount)) {
		super.depositCheck(amount, accountNo, routingNo);
		this.awardForTwentyK(amount);
		}
	}

	public void savAccountAdditionalFee(double chargePercentage) { // 20% fee if balance is below 10K

		this.setBalance(this.getBalance() - (this.getBalance() * (.20)));
	}

	public void minSavAccLimitFee(double amount) { // method above(20% fee if balance balance is below 10K) is combined
													// with $35 fee for balance below 10K.
													// but I couldn't
		if (this.checkBalance() - amount < minimumBalanceAmount) {
			this.chargeFee(35);
			this.savAccountAdditionalFee(.20);
			System.out.println("minumum balance fee of $35 and 20% applied");
		}
	}

	public void awardForTwentyK(double amount) { //method that checks if account is qualified for 5% reward
		if (this.checkBalance() >= awardAmount) {

			this.setBalance(this.getBalance() + (this.getBalance() * 0.05));
			System.out.println("Congratulations! Your balance is above $20,000 and qualified for 5% reward ");

		} else {

			System.out.println("deposit more money to reach $20K balance to 5% cash reward ");
		}
	}

	public void CheckIfAwardTakenAway(double amount) {
		if (this.checkBalance() - amount <= awardAmount) {
			this.setBalance(this.getBalance() - (this.getBalance() * 0.10));
			System.out.println("award is taken away, Saving Account balance is below 20K");

		}
	}
	public boolean checkAccBalMinimumThreashold(double amount) {
		if(this.checkBalance()- amount > this.accountBalanceMinimumThreshold) {
			return true;
		} else {
			System.out.println("Your final balance can not be less than 10000");
			return false;
		}
	}
	
	
	

	@Override
	public void withdrawCash(double amount) {
		if (minimumSavingAccountLimit(amount) && checkAccBalMinimumThreashold(amount) ) {
			super.withdrawCash(amount);
			this.CheckIfAwardTakenAway(amount);
			this.minSavAccLimitFee(amount);
		}

	}

	@Override
	public void transferFundsInternally(double amount, String accountNumber) {
		if (minimumSavingAccountLimit(amount) && checkAccBalMinimumThreashold(amount)) {
			super.transferFundsInternally(amount, accountNumber);
			this.CheckIfAwardTakenAway(amount);

		}

	}

}