package com.codistan.java.lesson10;

public class BankAccountRunner {
//we use main method
	public static void main(String[] args) {
		
		BankAccount myBankAccount = new BankAccount("00000", 0, "111223333");
		myBankAccount.depositCash(300);
		System.out.println(myBankAccount.checkBalance());
		
		myBankAccount.withdrawCash(110);
		myBankAccount.checkBalance(); //we designed checkBalance method to System.out.println(this.getBalance());
	}

}
