package com.codistan.java.lesson10;

public class SavingAccountRunner {

	public static void main(String[] args) {
		SavingAccount mySavingAccount = new SavingAccount("02020202", 0, "222233333");
mySavingAccount.depositCash(100);
mySavingAccount.checkBalance();


mySavingAccount.depositCash(10000);
mySavingAccount.checkBalance();

mySavingAccount.depositCash(10000);
mySavingAccount.checkBalance();

mySavingAccount.withdrawCash(10000);
mySavingAccount.checkBalance();
mySavingAccount.checkBalance();

mySavingAccount.withdrawCash(1000);
mySavingAccount.checkBalance();


	}

}
