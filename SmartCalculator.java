package com.codistan.java.lesson10;

public class SmartCalculator extends SimpleCalculator { //we are inheriting from SimpleCalculator(parent) class into SmartCalculatorClass(child)

//right click, source, create constructor from superclass. selected SimpleCalculator
	
	
public double squareArea(double num) {
		
		double numberSquare = num*num;
		return numberSquare;
		
		//return num*num;
		
		
	}
	
	public double circleArea(double r) {
			double area = r * r * Math.PI;
			return area;
	//return r * r * Math.PI; so you have a one line
	}
	
	public double addNumbers(double b, double c) { //example of overriding
		System.out.println("overriding");
		return b+c;
	}
	
	public double addNumbers(double a, double b, double c, double d) {
		System.out.println("overloading");
		return a+b+c+d;
	}

	public SmartCalculator() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SmartCalculator(double m, double minInputLimit, double maxInputLimit) {
		super(m, minInputLimit, maxInputLimit);
		// TODO Auto-generated constructor stub
	}

	public SmartCalculator(double min, double max) {
		super(min, max);
		// TODO Auto-generated constructor stub
	}

	public SmartCalculator(double m) {
		super(m);
		// TODO Auto-generated constructor stub	
		
	}
	
 public String tooString()
 {
	 return "This is a very smart calculator"; //overriding example.
 }
}
//what is the difference between overriding and overloading:
//overloading: same method name, different signatures, overriding :same method names, same signatures. 
//There is no overriding within the same class. it has to be in another class. (in child class. it doesn't have to be the child class, it can be grandchild as well)
//remember, the child class inherits from parent class example: public class SmartCalculator extends SimpleCalculator { 